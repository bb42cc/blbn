# Dockerfile

# preparation
FROM nginx:latest

ENV PYTHONUNBUFFERED 1

RUN mkdir -p /d/bluebin/pid
RUN mkdir -p /d/bluebin/sock
RUN mkdir -p /d/bluebin/uwsgi
RUN mkdir -p /d/bluebin/log
RUN mkdir -p /d/bluebin/src
RUN mkdir -p /d/bluebin/env
RUN mkdir -p /d/bluebin/conf/SSL
RUN mkdir -p /d/bluebin/media

# install system packages
RUN apt-get update && apt-get install -y \
    build-essential \
    python-virtualenv \
    python3-virtualenv \
    libssl-dev \
    python3-dev \
    git \
&& rm -rf /var/lib/apt/lists/*


# python related
WORKDIR /d/bluebin/src

COPY integration/ /d/bluebin/src/integration/
COPY accounts_app/ /d/bluebin/src/accounts_app/
COPY requisition_app /d/bluebin/src/requisition_app/
COPY belmero_bluebin/ /d/bluebin/src/belmero_bluebin/
COPY templates/ /d/bluebin/src/templates/
COPY static/ /d/bluebin/src/static/
COPY manage.py /d/bluebin/src/
COPY requirements.txt /d/bluebin/src/
COPY migrations.sh /d/bluebin/src/
COPY deployment/config/dotenv /d/bluebin/src/.env
COPY deployment/selfcert* /d/bluebin/conf/SSL/

COPY ./deployment/uwsgi.sh /d/bluebin/uwsgi/
COPY ./deployment/config/belmero-nginx.conf /d/bluebin/conf/belmero-nginx.conf
COPY ./deployment/config/nginx.conf /etc/nginx/nginx.conf
RUN rm /etc/nginx/conf.d/*

RUN virtualenv --python /usr/bin/python3 /d/bluebin/env
RUN /d/bluebin/env/bin/pip install -r /d/bluebin/src/requirements.txt
RUN /d/bluebin/env/bin/python3 manage.py collectstatic --noinput

RUN nginx -c /etc/nginx/nginx.conf
CMD sleep 20 && sh /d/bluebin/uwsgi/uwsgi.sh
