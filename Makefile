#
# constants
#

SHELL=/bin/bash

PROJECT_NAME=belmero
BIND_TO=0.0.0.0
RUNSERVER_PORT=8000
DEVSERVER_PORT=8001
SETTINGS=belmero_bluebin.settings
TEST_SETTINGS=supplier_returns.test_settings
TEST_APP?=returns_app accounts_app
flake8=flake8 --max-complexity=6 --exclude '*migrations*'

PYTHONPATH=$(CURDIR)

MANAGE_PREFIX= PYTHONPATH=$(PYTHONPATH) DJANGO_SETTINGS_MODULE=$(SETTINGS)
MANAGE_CMD=./manage.py
MANAGE= PYTHONPATH=$(PYTHONPATH) DJANGO_SETTINGS_MODULE=$(SETTINGS) $(MANAGE_CMD)


-include Makefile.def

#
# end of constants
#

#
# targets
#

.PHONY: run syncdb initproject dumpdata shell flake8 djangotest collectstatic clean manage migrate only_migrate init_migrate

run:
	@echo Starting $(PROJECT_NAME)...
	$(MANAGE) runserver $(BIND_TO):$(RUNSERVER_PORT)

syncdb:
	@echo Syncing...
	$(MANAGE) migrate --noinput
	@echo Done

initproject: syncdb loaddata


shell:
	@echo Starting shell...
	$(MANAGE) shell

flake8:
	$(flake8) --ignore=E501,C901 --exclude=.env,manage.py,.virtualenv $(TEST_APP)

djangotest:
	TESTING=1 PYTHONWARNINGS=ignore $(MANAGE) test --settings=$(TEST_SETTINGS) $(TEST_APP) -v 2

coverage:
	TESTING=1 PYTHONWARNINGS=ignore $(MANAGE_PREFIX) coverage run --source . \
	    $(MANAGE_CMD) test --settings=$(TEST_SETTINGS) $(TEST_APP) -v 2 && \
	    (coverage html --fail-under=100 || coverage report --fail-under=100)

test: flake8 djangotest

collectstatic:
	@echo Collecting static
	$(MANAGE) collectstatic --noinput
	@echo Done

clean:
	@echo Cleaning up...
	find ./ -name '__pycache__' -print|xargs -I {} rm -r {}
	@echo Done

only_migrate:
ifndef APP_NAME
	@echo Please, specify -e APP_NAME=appname argument
else
	@echo Starting of migration of $(APP_NAME)
	$(MANAGE) migrate $(APP_NAME)
	@echo Done
endif

migrate:
ifndef APP_NAME
	@echo "You can also specify -e APP_NAME='app' to check if new migrations needed for some app"
	$(MANAGE) migrate
else
	@echo Starting of migration of $(APP_NAME)
	$(MANAGE) schemamigration $(APP_NAME) --auto
	$(MANAGE) migrate $(APP_NAME)
	@echo Done
endif

init_migrate:
ifndef APP_NAME
	@echo Please, specify -e APP_NAME=appname argument
else
	@echo Starting init migration of $(APP_NAME)
	$(MANAGE) schemamigration $(APP_NAME) --initial
	$(MANAGE) migrate $(APP_NAME)
	@echo Done
endif

loaddata:
	$(MANAGE) loaddata requisition_app/fixtures/initial_data.json
	$(MANAGE) loaddata accounts_app/fixtures/initial_data.json
#
# end targets
