from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm  #


class UserCreateForm(UserCreationForm):

    class Meta:  # metadata
        fields = ('username', 'email', 'password1', 'password2')
        model = get_user_model()

    # this creates field labels for the fields listed above
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = 'Display Name'
        self.fields['email'].label = "Email Address"
