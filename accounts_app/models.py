# from django.db import models
from django.contrib import auth


# Create your models here.
class User(auth.models.User, auth.models.PermissionsMixin):

    def __str__(self):
        # username is a builtin var in auth.models.username,
        # this is the same User model you see in the Admin console
        return "@{}".format(self.username)
