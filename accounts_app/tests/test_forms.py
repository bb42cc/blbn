from django.test import TestCase

from accounts_app import forms


class UserCreateFormTestCase(TestCase):

    def test_user_create_form_invalid(self):
        """
        Check usercreatefrom with incorrect data input
        """
        form = forms.UserCreateForm({
            'username': ')(^%$WQ!)',
            'email': ')(*&^%$#@!)',
            'password1': '0987654321',
            'password2': '0987654321'
        })
        self.assertFalse(form.is_valid())

    def test_user_create_form_errors(self):
        """
        Check UserCreateForm's errors on incorrect data input
        """
        form = forms.UserCreateForm({
            'username': ')(^%$WQ!)',
            'email': ')(*&^%$#@!)',
            'password1': '0987654321',
            'password2': '0987654321'
        })
        expected = {
            'email': ['Enter a valid email address.'],
            'username': [
                'Enter a valid username. '
                'This value may contain only letters, numbers, and @'
                '/./+/-/_ characters.'
            ],
            'password2': ['This password is entirely numeric.']
        }

        errors = form.errors
        for field in errors:
            self.assertEqual(errors.get(field), expected.get(field))

    def test_user_create_form_valid(self):
        """
        Check usercreatefrom with correct data input
        """
        form = forms.UserCreateForm({
            'username': 'testusername_Lin',
            'email': 'testusername_Lin@mytestmail.com',
            'password1': 'qwerty1234',
            'password2': 'qwerty1234'
        })
        self.assertTrue(form.is_valid())

    def test_user_create_form_labels(self):
        """
        Check fields have proper custom labels
        """
        form = forms.UserCreateForm({
            'username': 'testusername_Lin',
            'email': 'testusername_Lin@mytestmail.com',
            'password1': 'qwerty1234',
            'password2': 'qwerty1234'
        })
        self.assertEqual(form.fields['username'].label, 'Display Name')
        self.assertEqual(form.fields['email'].label, 'Email Address')
