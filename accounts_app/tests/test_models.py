from django.test import TestCase

from accounts_app import models


class UserModelTestCase(TestCase):

    def setUp(self):
        self.user = models.User.objects.create(username='testusername_Lin')

    def test_user_str_perk(self):
        '''
        Check that User instance can be treated as string with expected value
        '''
        value = str(self.user)
        expected = '@{}'.format(self.user.username)
        self.assertEqual(value, expected)
