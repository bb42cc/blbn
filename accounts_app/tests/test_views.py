import base64

from unittest import skip

from django.test import TestCase
from django.urls import reverse


class AccountTemplatesTestCase(TestCase):
    """
    Check that basic urls of the app uses proper templates for response
    """

    def test_signup_template(self):
        response = self.client.get(reverse('accounts_app:signup'))
        self.assertTemplateUsed(response, 'accounts/signup.html')

    @skip('Broken logic')
    def test_signup_redirects_success(self):
        response = self.client.post(reverse('accounts_app:signup'), {
            'username': 'aaaa',
            'email': 'bbb@ccc.ddd',
            'password1': 'qwerty0987654321',
            'password2': 'qwerty0987654321'
        })
        self.assertRedirects(response, reverse('accounts_app:login'))

    def test_login_template(self):
        response = self.client.get(reverse('accounts_app:login'))
        self.assertTemplateUsed(response, 'accounts/login.html')

    def test_password_reset_template(self):
        response = self.client.get(reverse('accounts_app:password_reset'))
        self.assertTemplateUsed(response, 'accounts/password_reset_form.html')

    def test_password_resert_done_template(self):
        response = self.client.get(reverse('accounts_app:password_reset_done'))
        self.assertTemplateUsed(response, 'accounts/password_reset_done.html')

    def test_password_resert_confirm_template(self):
        response = self.client.get(reverse(
            'accounts_app:password_reset_confirm',
            args=[base64.urlsafe_b64encode(b'test'), 'test']
        ))
        self.assertTemplateUsed(
            response, 'accounts/password_reset_confirm.html')

    def test_password_resert_complete_template(self):
        response = self.client.get(reverse(
            'accounts_app:password_reset_complete'
        ))
        self.assertTemplateUsed(
            response, 'accounts/password_reset_complete.html')
