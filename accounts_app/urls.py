from django.urls import path
from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy

from accounts_app import views


app_name = 'accounts_app'

urlpatterns = [
    # calls the builtin login form, template=html file which is required
    path('login/', auth_views.LoginView.as_view(
        template_name='accounts/login.html'),
        name='login'),
    # this automatically goes to the home page once the user signs out
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('signup/', views.SignUp.as_view(), name='signup'),

    path('password-reset/', auth_views.PasswordResetView.as_view(
        template_name='accounts/password_reset_form.html',
        email_template_name='accounts/password_reset_email.html',
        success_url=reverse_lazy('accounts_app:password_reset_done')),
        name='password_reset'),
    path('password-reset-done/', auth_views.PasswordResetDoneView.as_view(
        template_name='accounts/password_reset_done.html'),
        name='password_reset_done'),
    path('password-reset-confirm/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(
             template_name='accounts/password_reset_confirm.html',
             success_url=reverse_lazy('accounts_app:password_reset_complete')),
         name='password_reset_confirm'),
    path('password-reset-complete/',
         auth_views.PasswordResetCompleteView.as_view(
             template_name='accounts/password_reset_complete.html'),
         name='password_reset_complete'),

]
