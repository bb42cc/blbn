# from django.shortcuts import render
# waits to transfer user until after they have hit the submit button
from django.urls import reverse_lazy
from django.views.generic import CreateView
from accounts_app import forms


# Create your views here.
# create view is used for creating new row/data using a form
class SignUp(CreateView):
    form_class = forms.UserCreateForm
    # on a successful signup redirect to login page
    success_url = reverse_lazy('login')
    template_name = 'accounts/signup.html'
