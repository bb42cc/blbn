from belmero_bluebin.settings import *  # pragma: no cover

import os

SECRET_KEY = 'AAAAAAAAAAAAAAA'
DEBUG = True
ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
