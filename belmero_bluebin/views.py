from django.shortcuts import render
from django.http import HttpResponseRedirect
# from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required


User = get_user_model()


@login_required
def index(request):
    return render(request, 'index.html')


@login_required
def logged_in(request):
    # return render(request, 'logged_in.html')
    # return render(request, 'index.html')
    return HttpResponseRedirect('/')


def logged_out(request):
    return render(request, 'logged_out.html')
