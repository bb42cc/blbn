#!/usr/bin/env bash

set -e

DOCKER_REPO="bluebin/bluebin"
DOCKER_TAG="latest"
DOCKER_PATH="${DOCKER_REPO}:${DOCKER_TAG}"

# Those vars should be base64 encoded
echo $SSLCERT | base64 -d > deployment/selfcert.crt
echo $SSLKEY | base64 -d > deployment/selfcert.key

# the variables are exported in circleci admin ui
docker login -u ${DOCKERHUB_USERNAME} -p ${DOCKERHUB_PASSWORD}
echo "Build & Push ${DOCKER_PATH}"
docker build -f Dockerfile -t ${DOCKER_PATH} .
docker push ${DOCKER_PATH}
