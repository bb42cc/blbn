#!/usr/bin/env sh

export LANG=en_US.utf8

deploy_dir='/d/bluebin'

env_path="${deploy_dir}/env"
sock_path="${deploy_dir}/sock/main.sock"
pid_path="${deploy_dir}/pid/main.pid"
wsgi_path="${deploy_dir}/src/belmero_bluebin"
log_path="${deploy_dir}/log/uwsgi.log"

if [ -f ${pid_path} ]
then
    echo Killing process `cat ${pid_path}`
    kill -9 `cat ${pid_path}`
    rm -f ${pid_path}
fi

if [ -f ${sock_path} ]
then
    rm -f ${sock_path}
fi

echo "Starting uwsgi process"
${env_path}/bin/uwsgi \
    --home ${env_path} \
    --socket ${sock_path} \
    --pidfile ${pid_path} \
    --pythonpath ${wsgi_path} \
    --module wsgi \
    --chmod-socket \
    --master \
    --no-orphans \
    --processes 2 \
    --enable-threads \
    --post-buffering 1 \
    --buffer-size 65535 \
    --daemonize ${log_path}

# start nginx
nginx -c /etc/nginx/nginx.conf -g "daemon off;"
