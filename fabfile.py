#!/usr/bin/env python3
import os

from fabric import task

REPOSITORY_URL = 'git@bitbucket.org:coffebelmero/blmr.git'

SITE_FOLDER_PATH = '/d/return-supplies/'
SOURCE_FOLDER_PATH = os.path.join(SITE_FOLDER_PATH, 'src')
VENV_FOLDER_PATH = os.path.join(SITE_FOLDER_PATH, 'env')
UWSGI_PID_PATH = os.path.join(SITE_FOLDER_PATH, 'pid', 'main.pid')



@task
def deploy(c):
    get_latest_source(c)
    update_virtualenv(c)
    run_migrations(c)
    collect_static_files(c)
    restart_webserver(c)


@task
def uname(c):
    c.run('uname -a')
    git_repo = os.path.join(SOURCE_FOLDER_PATH, '.git')
    q = c.run('test -d {}'.format(git_repo), echo=True).return_code
    c.local('echo {}'.format(q))


def get_latest_source(c):
    git_repo = os.path.join(SOURCE_FOLDER_PATH, '.git')

    if c.run('test -d {}'.format(git_repo), echo=True).failed:
        c.run(
            'git clone {} {}'.format(REPOSITORY_URL, SOURCE_FOLDER_PATH),
            echo=True
        )
    else:
        c.run('cd {} && git fetch'.format(SOURCE_FOLDER_PATH))
    current_commit = c.local('git log -n 1 --format=%H', echo=True).stdout
    c.run(
        'cd {} && git reset --hard {}'.format(
            SOURCE_FOLDER_PATH,
            current_commit,
        ), echo=True
    )


def update_virtualenv(c):
    if c.run('test -d {}'.format(VENV_FOLDER_PATH), echo=True).failed:
        c.run(
            'virtualenv --python /usr/bin/python3 {}'.format(VENV_FOLDER_PATH),
            echo=True
        )
    c.run(
        '{}/bin/pip install -r {}/requirements.txt'.format(
            VENV_FOLDER_PATH,
            SOURCE_FOLDER_PATH,
        ), echo=True
    )


def run_migrations(c):
    c.run(
        'cd {} && {}/bin/python manage.py migrate'.format(
            SOURCE_FOLDER_PATH,
            VENV_FOLDER_PATH,
        ), echo=True
    )


def collect_static_files(c):
    c.run(
        'cd {} && {}/bin/python manage.py collectstatic --noinput'.format(
            SOURCE_FOLDER_PATH,
            VENV_FOLDER_PATH,
        ), echo=True
    )


def restart_webserver(c):
    c.run(
        '{}/bin/uwsgi --reload {}'.format(VENV_FOLDER_PATH, UWSGI_PID_PATH),
        echo=True
    )
