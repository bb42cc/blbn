#!/usr/bin/env python

import click
import tempfile
import time
from jinja2 import FileSystemLoader, Environment
from kubernetes import client, config, utils
from kubernetes.client.rest import ApiException
from django.core.management.utils import get_random_secret_key

config.load_kube_config('deployment/k8s.config')

DEFAULT_SECRET_NAME = 'bluebin-docker-hub-secret'
SECRET_KEY = get_random_secret_key()
DEFAULT_NAMESPACES = [
    'default',
    'kube-public',
    'kube-system',
]


@click.group()
def cli():
    pass


def copy_secret(api, name):
    result = api.read_namespaced_secret(DEFAULT_SECRET_NAME, 'default')
    secret = client.V1Secret(
        metadata={'name': result.metadata.name},
        data=result.data,
        type=result.type
    )
    try:
        return api.create_namespaced_secret(name, secret)
    except ApiException as e:
        if e.reason == 'Conflict':
            click.echo('{} secret is already exists in {} namespace'.format(DEFAULT_SECRET_NAME, name))
            return False
        else:
            click.echo("Exception during coping docker hub secret:\n{}".format(e))
            return False


def create_resource(api, name, template_name):
    env = Environment(loader=FileSystemLoader(['deployment/kubernetes/']))
    template = env.get_template(template_name)
    result = template.render(name=name, secretkey=SECRET_KEY)

    resource_tmp_file = tempfile.NamedTemporaryFile(mode='w+')
    resource_tmp_file.write(result)
    resource_tmp_file.flush()
    resource_tmp_file.seek(0)
    try:
        return utils.create_from_yaml(api, resource_tmp_file.name)
    except ApiException as e:
        if e.reason == 'Conflict':
            click.echo('Resource is already exists in {} namespace'.format(name))
            return False
        else:
            click.echo("Exception during creating new resource:\n{}".format(e))
            return False


def create_namespace(api, name):
    namespace = client.V1Namespace(metadata={
        'name': name
    })

    try:
        return api.create_namespace(namespace, include_uninitialized=True)
    except ApiException as e:
        if e.reason == 'Conflict':
            click.echo('{} installation is already exists'.format(name))
            return False
        else:
            click.echo("Exception during creating new namespace:\n{}".format(e))
            return False


def print_service_details(api, name):
    instance = '{}-instance'.format(name)
    try:
        timer = 0
        while timer < 6:
            result = api.read_namespaced_service(instance, name, pretty=True)
            if result.status.load_balancer.ingress:
                break
            timer += 1
            click.echo('Service not ready, waiting 10s (up to 60s max)')
            time.sleep(10)
    except ApiException as e:
        click.echo("Exception trying obtain service details:\n{}".format(e))
        return False

    metadata = result.metadata
    ingress = result.status.load_balancer.ingress
    click.echo('\nService name: {}'.format(metadata.name))
    click.echo('Namespace: {}'.format(metadata.namespace))
    if ingress:
        click.echo('IP (for DNS): {}'.format(ingress))
    else:
        click.echo('Service still not ready, please check details for debug')
    return True


@cli.command('deploy')
@click.argument('name', nargs=1)
def deploy(name):
    api_client = client.ApiClient()
    v1_core_api = client.CoreV1Api()
    result = create_namespace(v1_core_api, name)
    if not result:
        exit(1)
    copy_secret(v1_core_api, name)

    creation_sequence = [
        'db_pvc.yml.jinja2',
        'app_pvc.yml.jinja2',
        'deployment.yml.jinja2',
        'service.yml.jinja2'
    ]

    last_result = None
    for res_type in creation_sequence:
        last_result = create_resource(api_client, name, res_type)
        if not last_result:
            exit(1)
    click.echo('Creation finished')
    if not print_service_details(v1_core_api, name):
        exit(1)
    exit(0)


def list_namespaces(api):
    try:
        return api.list_namespace().items
    except ApiException as e:
        click.echo("Exception during obtaining namespaces:\n{}".format(e))
        return False


def flip_image_name(name):
    suffix = ':latest'
    if suffix in name:
        return name.replace(suffix, '')
    return '{}{}'.format(name, suffix)


def get_index(containers, name='bluebinapp'):
    index = 0
    for cont in containers:
        if name == cont.name:
            return index
        index += 1
    return -1


def update_namespace_app(api, namespace):
    deployment_name = "{}-deployment".format(namespace)
    try:
        deployment = api.read_namespaced_deployment(deployment_name, namespace)
    except ApiException as e:
        click.echo("Exception during obtaining deployment:\n{}".format(e))
        return False

    index = get_index(deployment.spec.template.spec.containers)
    if index == -1:
        click.echo("bluebinapp does not present in deployment")
        return False

    image = deployment.spec.template.spec.containers[index].image
    deployment.spec.template.spec.containers[index].image = flip_image_name(image)
    try:
        return api.patch_namespaced_deployment(deployment_name, namespace, deployment)
    except ApiException as e:
        click.echo("Exception during updating deployment:\n{}".format(e))
        return False


@cli.command('update')
@click.argument('names', nargs=-1)
def update(names):
    v1_core_api = client.CoreV1Api()
    appv1 = client.AppsV1Api()

    namespaces_objects = list_namespaces(v1_core_api)
    if not namespaces_objects:
        exit(1)
    namespaces = map(lambda x: x.metadata.name, namespaces_objects)
    if not names:
        targets = list(filter(lambda x: x not in DEFAULT_NAMESPACES, namespaces))
    else:
        if not all(map(lambda x: x in namespaces, names)):
            not_present = list(map(lambda x: x not in namespaces, names))
            click.echo("Namespaces {} does not exists".format(not_present))
            exit(1)
        targets = names

    if not targets:
        click.echo('Nothing to update')
        exit(0)

    for namespace in targets:
        click.echo('Updating {}'.format(namespace))
        if not update_namespace_app(appv1, namespace):
            exit(1)
    click.echo('Update finished')
    exit(0)


if __name__ == '__main__':
    cli()
