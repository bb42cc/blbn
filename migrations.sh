#!/bin/bash


/d/bluebin/env/bin/python manage.py migrate
/d/bluebin/env/bin/python manage.py loaddata requisition_app/fixtures/initial_data.json
/d/bluebin/env/bin/python manage.py loaddata accounts_app/fixtures/initial_data.json
