from django.contrib import admin
from requisition_app import models
from requisition_app import forms


class RequisitionIntegrationSetupAdminForm(admin.ModelAdmin):
    form = forms.RequisitionIntegrationSetupForm


class RequisitionIntegrationFileHdrFieldsAdminForm(admin.ModelAdmin):
    form = forms.RequisitionIntegrationFileHdrFieldsForm


class RequisitionIntegrationFileLineFieldsAdminForm(admin.ModelAdmin):
    form = forms.RequisitionIntegrationFileLineFieldsForm


# Register your models here.
admin.site.register(models.ReqInstallation)
admin.site.register(models.ReqIntegrationSetup,
                    RequisitionIntegrationSetupAdminForm)
admin.site.register(models.ReqHeader)
admin.site.register(models.ReqLine)
admin.site.register(models.ReqIntegrationFileHdrFields,
                    RequisitionIntegrationFileHdrFieldsAdminForm)
admin.site.register(models.ReqIntegrationFileLineFields,
                    RequisitionIntegrationFileLineFieldsAdminForm)
