from django.apps import AppConfig


class RequisitionAppConfig(AppConfig):
    name = 'requisition_app'
