import django_filters
from requisition_app import models
from requisition_app.views import remove_leading_asterisk


class RequisitionFilter(django_filters.FilterSet):
    id = django_filters.CharFilter(lookup_expr='icontains')
    requester_id = django_filters.CharFilter(lookup_expr='icontains')
    business_unit = django_filters.CharFilter(lookup_expr='icontains')
    location = django_filters.CharFilter(lookup_expr='icontains')

    start_date = django_filters.DateTimeFilter(
        name='entered_date', lookup_expr='gte',
        label="Enter From Date mm/dd/yyyy")
    end_date = django_filters.DateTimeFilter(
        name='entered_date', lookup_expr='lte',
        label="Enter To Date mm/dd/yyyy")

    custom_field_c10 = django_filters.CharFilter(lookup_expr='icontains')
    custom_field_c30 = django_filters.CharFilter(lookup_expr='icontains')
    custom_field_c60 = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = models.ReqHeader
        fields = [
            'id', 'requester_id', 'business_unit', 'location',
            'start_date', 'end_date',
            'custom_field_c10', 'custom_field_c30', 'custom_field_c60'
        ]

    def handle_custom_field(self, installation, field):
        if getattr(installation, 'req_hdr_{}_hide_field'.format(field)):
            del self.filters[field]
        else:
            self.filters[field].label = remove_leading_asterisk(
                getattr(installation, 'req_hdr_{}_label'.format(field))
            )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        installation = models.ReqInstallation.objects.first()

        self.handle_custom_field(installation, 'custom_field_c10')
        self.handle_custom_field(installation, 'custom_field_c30')
        self.handle_custom_field(installation, 'custom_field_c60')

        self.filters['business_unit'].label = remove_leading_asterisk(
            installation.business_unit_label)
        self.filters['location'].label = remove_leading_asterisk(
            installation.location_label)


    @property
    def qs(self):
        for f in self.filters.keys():
            if self.data.get(f, None):
                return super().qs
        return self.Meta.model.objects.none()
