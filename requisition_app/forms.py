from django.forms import (ModelForm, PasswordInput, ChoiceField,
                          ModelChoiceField)

from requisition_app import models


class RequisitionIntegrationSetupForm(ModelForm):
    class Meta:
        model = models.ReqIntegrationSetup
        fields = '__all__'
        widgets = {
            'sftp_password': PasswordInput(render_value=True),
            'bluebin_sftp_password': PasswordInput(render_value=True),
        }


class RequisitionIntegrationFileHdrFieldsForm(ModelForm):
    field_name = ChoiceField(choices=[
        (f.name, f.name) for f in models.ReqHeader._meta.get_fields()][1:])

    class Meta:
        model = models.ReqIntegrationFileHdrFields
        fields = '__all__'


class RequisitionIntegrationFileLineFieldsForm(ModelForm):
    field_name = ChoiceField(choices=[
        (f.name, f.name) for f in models.ReqLine._meta.get_fields()])

    class Meta:
        model = models.ReqIntegrationFileLineFields
        fields = '__all__'


class RequisitionHdrForm(ModelForm):
    # requisition header model form
    def __init__(self, *args, **kwargs):
        super(RequisitionHdrForm, self).__init__(*args, **kwargs)

        # retrieve field values from requisition installation ModelForm
        reqinstallation = models.ReqInstallation.objects.first()

        # set customer specific labels and placeholder text
        self.fields['requester_id'].label = reqinstallation.requester_id_label
        self.fields['requester_id'].widget.attrs.update({
            'placeholder': reqinstallation.requester_id_placeholder})

        self.fields['business_unit'].label =\
            reqinstallation.business_unit_label
        self.fields['business_unit'].widget.attrs.update({
            'autofocus': 'autofocus',
            'placeholder': reqinstallation.business_unit_placeholder})

        self.fields['location'].label =\
            reqinstallation.location_label
        self.fields['location'].widget.attrs.update({
            'placeholder': reqinstallation.location_placeholder})

        self.fields['custom_field_c10'].label =\
            reqinstallation.req_hdr_custom_field_c10_label
        self.fields['custom_field_c10'].widget.attrs.update({
            'placeholder': reqinstallation.req_hdr_custom_field_c10_label})

        if reqinstallation.req_hdr_custom_field_c10_hide_field:
            self.fields['custom_field_c10'].required = False
        else:  # if not hidden check required field, may not be required
            if reqinstallation.req_hdr_custom_field_c10_required_field:
                self.fields['custom_field_c10'].required = True
            else:
                self.fields['custom_field_c10'].required = False

        self.fields['custom_field_c30'].label =\
            reqinstallation.req_hdr_custom_field_c10_label
        self.fields['custom_field_c30'].widget.attrs.update({
            'placeholder': reqinstallation.req_hdr_custom_field_c10_label})

        if reqinstallation.req_hdr_custom_field_c10_hide_field:
            self.fields['custom_field_c30'].required = False
        else:  # if not hidden check required field, may not be required
            if reqinstallation.req_hdr_custom_field_c10_required_field:
                self.fields['custom_field_c30'].required = True
            else:
                self.fields['custom_field_c30'].required = False

        self.fields['custom_field_c60'].label =\
            reqinstallation.req_hdr_custom_field_c10_label
        self.fields['custom_field_c60'].widget.attrs.update({
            'placeholder': reqinstallation.req_hdr_custom_field_c10_label})

        if reqinstallation.req_hdr_custom_field_c10_hide_field:
            self.fields['custom_field_c60'].required = False
        else:  # if not hidden check required field, may not be required
            if reqinstallation.req_hdr_custom_field_c10_required_field:
                self.fields['custom_field_c60'].required = True
            else:
                self.fields['custom_field_c60'].required = False

        # disable field
        self.fields['requester_id'].disabled = True

    class Meta:
            model = models.ReqHeader
            # exclude = ('return_status',)
            # ModelForm requires either fields or exclude statement
            # fields = '__all__'
            exclude = [
                'record_status',
                'interfaced',
                'last_interface_date'
            ]


class RequisitionHdrReadOnlyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RequisitionHdrReadOnlyForm, self).__init__(*args, **kwargs)

        reqinstallation = models.ReqInstallation.objects.first()

        # set customer specific labels
        # set customer specific labels and placeholder text
        self.fields['requester_id'].label = reqinstallation.requester_id_label

        self.fields['business_unit'].label =\
            reqinstallation.business_unit_label

        self.fields['location'].label =\
            reqinstallation.location_label

        self.fields['custom_field_c10'].label =\
            reqinstallation.req_hdr_custom_field_c10_label

        self.fields['custom_field_c30'].label =\
            reqinstallation.req_hdr_custom_field_c10_label

        self.fields['custom_field_c60'].label =\
            reqinstallation.req_hdr_custom_field_c10_label

        # disable all fields
        self.fields['business_unit'].disabled = True
        self.fields['location'].disabled = True
        self.fields['custom_field_c10'].disabled = True
        self.fields['custom_field_c30'].disabled = True
        self.fields['custom_field_c60'].disabled = True

    class Meta:
        model = models.ReqHeader
        exclude = [
            'record_status',
            'interfaced',
            'last_interface_date'
        ]


class RequisitionLineForm(ModelForm):
    # requisition line form, child to requisition header form
    def __init__(self, *args, **kwargs):
        super(RequisitionLineForm, self).__init__(*args, **kwargs)

        # retrieve field values from installation model
        reqinstallation = models.ReqInstallation.objects.first()

        # set customer specific labels and placeholder text
        self.fields['item_id'].label =\
            reqinstallation.item_id_label
        self.fields['item_id'].widget.attrs.update({
            'placeholder': reqinstallation.item_id_placeholder})

        self.fields['bin'].label =\
            reqinstallation.bin_label
        self.fields['bin'].widget.attrs.update({
            'placeholder': reqinstallation.bin_label})

        if reqinstallation.bin_hide_field:
            self.fields['bin'].required = False
        else:  # if not hidden check required field, may not be required
            if reqinstallation.bin_required_field:
                self.fields['bin'].required = True
            else:
                self.fields['bin'].required = False

        self.fields['qty'].label =\
            reqinstallation.qty_label
        self.fields['qty'].widget.attrs.update({
            'placeholder': reqinstallation.qty_label})

        self.fields['custom_field_c10'].label =\
            reqinstallation.req_line_custom_field_c10_label
        self.fields['custom_field_c10'].widget.attrs.update({
            'placeholder': reqinstallation.req_line_custom_field_c10_label})

        if reqinstallation.req_line_custom_field_c10_hide_field:
            self.fields['custom_field_c10'].required = False
        else:  # if not hidden check required field, may not be required
            if reqinstallation.req_line_custom_field_c10_required_field:
                self.fields['custom_field_c10'].required = True
            else:
                self.fields['custom_field_c10'].required = False

        self.fields['custom_field_c30'].label =\
            reqinstallation.req_line_custom_field_c10_label
        self.fields['custom_field_c30'].widget.attrs.update({
            'placeholder': reqinstallation.req_line_custom_field_c10_label})

        if reqinstallation.req_line_custom_field_c10_hide_field:
            self.fields['custom_field_c30'].required = False
        else:  # if not hidden check required field, may not be required
            if reqinstallation.req_line_custom_field_c10_required_field:
                self.fields['custom_field_c30'].required = True
            else:
                self.fields['custom_field_c30'].required = False

        self.fields['custom_field_c60'].label =\
            reqinstallation.req_line_custom_field_c10_label
        self.fields['custom_field_c60'].widget.attrs.update({
            'placeholder': reqinstallation.req_line_custom_field_c10_label})

        if reqinstallation.req_line_custom_field_c10_hide_field:
            self.fields['custom_field_c60'].required = False
        else:  # if not hidden check required field, may not be required
            if reqinstallation.req_line_custom_field_c10_required_field:
                self.fields['custom_field_c60'].required = True
            else:
                self.fields['custom_field_c60'].required = False

    class Meta:
        model = models.ReqLine
        fields = '__all__'


class RequisitionLineReadOnlyForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RequisitionLineReadOnlyForm, self).__init__(*args, **kwargs)

        reqinstallation = models.ReqInstallation.objects.first()

        # set customer specific labels and placeholder text
        self.fields['item_id'].label =\
            reqinstallation.item_id_label

        self.fields['bin'].label =\
            reqinstallation.bin_label

        self.fields['qty'].label =\
            reqinstallation.qty_label

        self.fields['custom_field_c10'].label =\
            reqinstallation.req_line_custom_field_c10_label

        self.fields['custom_field_c30'].label =\
            reqinstallation.req_line_custom_field_c10_label

        self.fields['custom_field_c60'].label =\
            reqinstallation.req_line_custom_field_c10_label

        # disable all fields
        self.fields['item_id'].disabled = True
        self.fields['bin'].disabled = True
        self.fields['qty'].disabled = True
        self.fields['custom_field_c10'].disabled = True
        self.fields['custom_field_c30'].disabled = True
        self.fields['custom_field_c60'].disabled = True

    class Meta:
        model = models.ReqLine
        exclude = [
            'record_status',
            'interfaced',
            'last_interface_date'
        ]
