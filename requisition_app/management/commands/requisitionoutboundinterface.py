"""
Outbound Integrations for Returns
SFTP or Email file attach
0.  Check to see if customer is setup for outbound integrations
1.  Check to see if there are any new Returns to interface
2.  Create same file created from the online Export page.  This process also
    updates the appropriate flags on the record_hdr model
3.  Send email attach if customer has enabled email integration
4.  Send sftp if customer has enabled sftp integration is True.  Must support:
    a. sftp with NO private Key
    b. sftp with private Key
    c. sftp with private key and private key password
Note: This will be setup as a Cron job per the customer's requested schedule
"""
import pysftp  # only use 0.2.8
import os
import csv
import copy

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from django.conf import settings
from django.core.mail import BadHeaderError, EmailMessage
from decouple import config

from requisition_app.models import ReqIntegrationSetup, ReqHeader
from requisition_app.views import (get_req_hdr_columns,
                                   get_req_line_columns,
                                   get_and_write_req_data,
                                   write_req_columns_to_file)

# system vars
base_dir = settings.BASE_DIR
integrationsetup = False


class Command(BaseCommand):
    help = 'Requisition Outbound Integration - sftp & email'

    def handle(self, *args, **kwargs):
        # Check to see if customer is setup for outbound integrations
        # If it is retrieve data
        # retrieve integration setup parms
        api_setup = ReqIntegrationSetup.objects.all()[0]

        # only run the export if at least one integration parm is True
        if api_setup.enable_sftp_integration \
            or api_setup.bluebin_enable_sftp_integration \
                or api_setup.enable_email_integration:
            # check to see if there are any returns to interfaced
            exists = ReqHeader.objects.filter(interfaced=False).exists()

            if exists:
                # set file name and path
                current_datetime = timezone.now()
                formatted_date = current_datetime.strftime("%Y%m%d-%H%M%S")
                file_name = (api_setup.file_name_prefix + str(formatted_date)
                             + "." + api_setup.file_name_extension)
                full_path_file_name = (os.path.join(base_dir, "integration",
                                                    str(file_name)))

                csvfile = open(full_path_file_name, 'w', newline='')
                writer = csv.writer(csvfile,
                                    delimiter=api_setup.file_delimiter,
                                    quotechar=api_setup.file_quote_character,
                                    quoting=csv.QUOTE_MINIMAL)

                # get Hdr & Line Columns & write to file
                req_hdr_fields_list = get_req_hdr_columns()
                export_header = copy.copy(req_hdr_fields_list)
                export_header.insert(0, "RowType")
                export_header = tuple(export_header)

                req_line_fields_list = get_req_line_columns()
                export_line = copy.copy(req_line_fields_list)
                export_line.insert(0, "RowType")
                export_line = tuple(export_line)

                if api_setup.include_header_rows_in_file is True:
                    writer = write_req_columns_to_file(writer, export_header,
                                                       export_line)

                # create export_data queryset, tuple list, ordered by id
                req_hdr_fields_list.insert(0, "id")  # need id for line lookup
                export_data = ReqHeader.objects.filter(
                    interfaced=False).values_list(
                        *req_hdr_fields_list).order_by('id')

                # retrieve line data and write hdr and line data to file
                interfacetype = "interface"
                export_option = None  # only applies for export interfacetype
                writer = get_and_write_req_data(
                    export_data, req_line_fields_list, writer, interfacetype,
                    export_option, api_setup)
                csvfile.close()

                # email interface Code
                if api_setup.enable_email_integration:
                    if api_setup.email_integration_email_address:
                        from_email = config('DEFAULT_FROM_EMAIL')
                        to_email = api_setup.email_integration_email_address
                        if api_setup.email_integration_cc_email_address:
                            cc_email = \
                                api_setup.email_integration_cc_email_address
                        else:
                            cc_email = ""
                        subject = \
                            str(api_setup.email_integration_email_subject)
                        message = \
                            str(api_setup.email_integration_email_body_intro)

                        try:
                            email = EmailMessage(
                                subject,    # Subject
                                message,    # Message/Body
                                from_email,
                                [to_email,
                                 cc_email],
                                reply_to=[from_email],
                                )

                            attachment = open(full_path_file_name, 'r')
                            email.attach(file_name, attachment.read())
                            attachment.close()

                            email.send(fail_silently=False)
                        except BadHeaderError:
                            raise CommandError(
                                'Invalid header found or error occurred')

                # Customer sftp processing
                if api_setup.enable_sftp_integration:
                    sftp_processing(api_setup.sftp_host_name,
                                    api_setup.sftp_username,
                                    api_setup.sftp_password,
                                    api_setup.sftp_port,
                                    api_setup.sftp_remote_file_path,
                                    file_name,
                                    full_path_file_name,
                                    api_setup.sftp_use_ssh_private_key,
                                    api_setup.sftp_private_key_password)

                # BlueBin sftp processing
                if api_setup.bluebin_enable_sftp_integration:
                    sftp_processing(api_setup.bluebin_sftp_host_name,
                                    api_setup.bluebin_sftp_username,
                                    api_setup.bluebin_sftp_password,
                                    api_setup.bluebin_sftp_port,
                                    api_setup.bluebin_sftp_remote_file_path,
                                    file_name,
                                    full_path_file_name,
                                    api_setup.bluebin_sftp_use_ssh_private_key,
                                    api_setup.bluebin_sftp_private_key_password
                                    )

                self.stdout.write(self.style.SUCCESS(
                    'success - Requisition Integration Completed Successfully')
                                  )
            else:
                message = 'notice - the process completed successfully'
                message = message + ' but there are no records to interface'
                self.stdout.write(self.style.NOTICE(message))

        else:
            message = 'notice - the process completed successfully'
            message = message + ' but there are no integrations setup'
            self.stdout.write(self.style.NOTICE(message))


def sftp_processing(host_name, user_name, password, port, remote_file_path,
                    file_name, full_path_file_name, use_ssh_private_key,
                    private_key_password):
    if (host_name is None or user_name is None or
        password is None or port is None or
            remote_file_path is None):
            print("Mising SFTP Setup Values...Unable to SFTP" +
                  str(file_name))
    else:
        try:
            # 3 scenarios; 1) use private key, 2) user private key
            # and key password & 3) no private key
            if use_ssh_private_key:
                # THIS NEEDS TO BE TESTED
                cnopts = pysftp.CnOpts()
                cnopts.hostkeys.load('known_hosts')
                if use_ssh_private_key:
                    private_key_pass =\
                        private_key_password
                    sftp = pysftp.Connection(
                        host=host_name, username=user_name,
                        password=password, port=port,
                        cnopts=cnopts,
                        private_key_password=private_key_pass)
                else:
                    # withOUT a private key password
                    sftp = pysftp.Connection(
                        host=host_name, username=user_name,
                        password=password, port=port,
                        cnopts=cnopts)

            else:
                sftp = pysftp.Connection(
                    host=host_name, username=user_name,
                    password=password, port=port)

            # set local and remote directories
            localpath = str(full_path_file_name)
            remotepath = str(remote_file_path) + str(file_name)

            # put file
            sftp.put(localpath, remotepath)
            # sftp.chdir(remote_file_path)
            # sftp.put(csvfile.name)

            sftp.close()
            sftp.connection = None

        except Exception:
            print("An error occurred during SFTP transfer")
            sftp.close()
            sftp.connection = None
