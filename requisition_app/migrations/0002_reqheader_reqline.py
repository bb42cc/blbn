# Generated by Django 2.0.2 on 2018-11-17 23:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('requisition_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReqHeader',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('requester_name', models.CharField(max_length=60)),
                ('business_unit', models.CharField(max_length=10)),
                ('location', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='ReqLine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item_id', models.CharField(help_text='Enter the Item Id', max_length=30)),
                ('bin', models.CharField(blank=True, help_text='Enter the Bin Number', max_length=1)),
                ('qty', models.DecimalField(decimal_places=2, max_digits=18)),
                ('parent_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='req_line_set', to='requisition_app.ReqHeader')),
            ],
        ),
    ]
