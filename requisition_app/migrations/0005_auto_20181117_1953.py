# Generated by Django 2.0.2 on 2018-11-18 01:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('requisition_app', '0004_auto_20181117_1951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reqinstallation',
            name='bin_label',
            field=models.CharField(default='*Bin A/B:', max_length=60),
        ),
        migrations.AlterField(
            model_name='reqinstallation',
            name='bin_placeholder',
            field=models.CharField(default='Enter the Bin A/B Id', max_length=60),
        ),
    ]
