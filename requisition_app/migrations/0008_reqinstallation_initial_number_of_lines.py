# Generated by Django 2.0.2 on 2018-11-20 03:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('requisition_app', '0007_auto_20181119_2119'),
    ]

    operations = [
        migrations.AddField(
            model_name='reqinstallation',
            name='initial_number_of_lines',
            field=models.IntegerField(default=99, help_text='This field determines how many lines default onto the        requisition page'),
        ),
    ]
