# from decimal import Decimal
from django.db import models
# from django.dispatch import receiver
# from django.urls import reverse
from django.utils import timezone


class ReqInstallation(models.Model):  # this table MUST only have one row
    requisition_form_name = models.CharField(
        max_length=60, blank=False, default="BlueBin Requisition Form",
        help_text="This is the name that shows up at the top of the req\
        form")

    requisition_customer_logo = models.FileField(
        upload_to='installation/', blank=True,
        help_text="this is the logo that displays\
        to the left of the form name.  IMPORTANT:\
        the image must be sized around 75 x 50\
        or it will not display correctly!")

    requisition_top_of_page_instructions = models.TextField(
        blank=True, default="Please complete the following form to requisition\
        materials",
        help_text="These are the instructions that show up in the grey box\
        at the top of the req form.  This is only text, no hyperlinks, etc")

    requester_id_label = models.CharField(
        max_length=60, blank=False, default="*Requester Id:",
        help_text="This is a required field so use a leading *")

    requester_id_placeholder = models.CharField(
        max_length=60, blank=False, default="Enter your ERP Requester Id")

    business_unit_label = models.CharField(
        max_length=60, blank=False, default="*Business Unit:",
        help_text="This is a required field so use a leading *")

    business_unit_placeholder = models.CharField(
        max_length=60, blank=False, default="Enter the Inv Business Unit")

    location_label = models.CharField(
        max_length=60, blank=False, default="*Par Location:",
        help_text="This is a required field so use a leading *")

    location_placeholder = models.CharField(
        max_length=60, blank=False, default="Enter the Par Location Id")

    req_hdr_custom_field_c10_label = models.CharField(
        max_length=60, blank=False, default="Custom Field C10")

    req_hdr_custom_field_c10_placeholder = models.CharField(
        max_length=60, blank=False, default="Custom Field C10")

    req_hdr_custom_field_c10_required_field = models.BooleanField(
        blank=False, default=False, help_text="Check the checkbox to require\
         a value in this field but this required validation is only applied\
         if the field is NOT hidden. If the field is hidden then the field\
         is NOT required whether this checkbox is checked or not")

    req_hdr_custom_field_c10_hide_field = models.BooleanField(
        blank=False, default=True, help_text="Check the checkbox to hide this\
        field. This field works in conjunction with the required field")

    req_hdr_custom_field_c30_label = models.CharField(
        max_length=60, blank=False, default="Custom Field C30")

    req_hdr_custom_field_c30_placeholder = models.CharField(
        max_length=60, blank=False, default="Custom Field C30")

    req_hdr_custom_field_c30_required_field = models.BooleanField(
        blank=False, default=False, help_text="Check the checkbox to require\
         a value in this field but this required validation is only applied\
         if the field is NOT hidden. If the field is hidden then the field\
         is NOT required whether this checkbox is checked or not")

    req_hdr_custom_field_c30_hide_field = models.BooleanField(
        blank=False, default=True, help_text="Check the checkbox to hide this\
        field. This field works in conjunction with the required field")

    req_hdr_custom_field_c60_label = models.CharField(
        max_length=60, blank=False, default="Custom Field C60")

    req_hdr_custom_field_c60_placeholder = models.CharField(
        max_length=60, blank=False, default="Custom Field C60")

    req_hdr_custom_field_c60_required_field = models.BooleanField(
        blank=False, default=False, help_text="Check the checkbox to require\
         a value in this field but this required validation is only applied\
         if the field is NOT hidden. If the field is hidden then the field\
         is NOT required whether this checkbox is checked or not")

    req_hdr_custom_field_c60_hide_field = models.BooleanField(
        blank=False, default=True, help_text="Check the checkbox to hide this\
        field. This field works in conjunction with the required field")

    initial_number_of_lines = models.IntegerField(
        blank=False, default=99,
        help_text="This field determines how many lines default onto the\
        requisition page")

    item_id_label = models.CharField(
        max_length=60, blank=False, default="*Item Id:",
        help_text="This is a required field so use a leading *")

    item_id_placeholder = models.CharField(
        max_length=60, blank=False, default="Enter the Item Id")

    bin_label = models.CharField(
        max_length=60, blank=False, default="*Bin A/B:")

    bin_placeholder = models.CharField(
        max_length=60, blank=False, default="Enter the Bin A/B Id")

    bin_required_field = models.BooleanField(
        blank=False, default=False, help_text="Check the checkbox to require\
         a value in this field but this required validation is only applied\
         if the field is NOT hidden. If the field is hidden then the field\
         is NOT required whether this checkbox is checked or not")

    bin_hide_field = models.BooleanField(
        blank=False, default=True, help_text="Check the checkbox to hide this\
        field. This field works in conjunction with the required field")

    qty_label = models.CharField(
        max_length=60, blank=False, default="*Quantity:",
        help_text="This is a required field so use a leading *")

    qty_placeholder = models.CharField(
        max_length=60, blank=False, default="Enter the Quantity")

    req_line_custom_field_c10_label = models.CharField(
        max_length=60, blank=False, default="Custom Field C10")

    req_line_custom_field_c10_placeholder = models.CharField(
        max_length=60, blank=False, default="Custom Field C10")

    req_line_custom_field_c10_required_field = models.BooleanField(
        blank=False, default=False, help_text="Check the checkbox to require\
         a value in this field but this required validation is only applied\
         if the field is NOT hidden. If the field is hidden then the field\
         is NOT required whether this checkbox is checked or not")

    req_line_custom_field_c10_hide_field = models.BooleanField(
        blank=False, default=True, help_text="Check the checkbox to hide this\
        field. This field works in conjunction with the required field")

    req_line_custom_field_c30_label = models.CharField(
        max_length=60, blank=False, default="Custom Field C30")

    req_line_custom_field_c30_placeholder = models.CharField(
        max_length=60, blank=False, default="Custom Field C30")

    req_line_custom_field_c30_required_field = models.BooleanField(
        blank=False, default=False, help_text="Check the checkbox to require\
         a value in this field but this required validation is only applied\
         if the field is NOT hidden. If the field is hidden then the field\
         is NOT required whether this checkbox is checked or not")

    req_line_custom_field_c30_hide_field = models.BooleanField(
        blank=False, default=True, help_text="Check the checkbox to hide this\
        field. This field works in conjunction with the required field")

    req_line_custom_field_c60_label = models.CharField(
        max_length=60, blank=False, default="Custom Field C60")

    req_line_custom_field_c60_placeholder = models.CharField(
        max_length=60, blank=False, default="Custom Field C60")

    req_line_custom_field_c60_required_field = models.BooleanField(
        blank=False, default=False, help_text="Check the checkbox to require\
         a value in this field but this required validation is only applied\
         if the field is NOT hidden. If the field is hidden then the field\
         is NOT required whether this checkbox is checked or not")

    req_line_custom_field_c60_hide_field = models.BooleanField(
        blank=False, default=True, help_text="Check the checkbox to hide this\
        field. This field works in conjunction with the required field")

    def __str__(self):
        return str("Requisition Installation - Only ONE row")


class ReqIntegrationSetup(models.Model):
    enable_sftp_integration = models.BooleanField(
        default=True, help_text="Check this checkbox to enable SFTP\
        integration and populate the corresponding SFTP setup fields")

    sftp_host_name = models.CharField(
        max_length=100, blank=True, null=True, help_text="Populate this field\
        with the hostname the file should be put to")

    sftp_username = models.CharField(
        max_length=100, blank=True, null=True, help_text="Populate this field\
        with the username that should be used for authentication purposes")

    sftp_password = models.CharField(
        max_length=100, blank=True, null=True, help_text="If applicable,\
        populate this field with the password that should be used for\
        authentication purposes")

    sftp_port = models.IntegerField(
        blank=True, null=True, help_text="Enter the port that\
        should be used to make the connection (default=22 for SFTP)")

    sftp_remote_file_path = models.CharField(
        max_length=100, blank=True, null=True, help_text="Enter the\
        file path of where the file should be written.  It's the path from the\
        default directory of the user we're signing in withself.\
        Don't forget the last slash.  Example: public_ftp/incoming/belmero/")

    sftp_use_ssh_private_key = models.BooleanField(
        default=False, help_text="Check this checkbox to use the ssh\
        private key.  This must be added to the known_hosts file in the .ssh\
        folder")

    sftp_private_key_password = models.CharField(
        max_length=100, blank=True, null=True, help_text="If applicable,\
        populate this field with the Private Key password that should be used\
        for authentication purposes.  Don't confuse this with the user\
        password")

    bluebin_enable_sftp_integration = models.BooleanField(
        default=True, help_text="Check this checkbox to enable SFTP\
        integration and populate the corresponding SFTP setup fields")

    bluebin_sftp_host_name = models.CharField(
        max_length=100, blank=True, null=True, help_text="Populate this field\
        with the hostname the file should be put to")

    bluebin_sftp_username = models.CharField(
        max_length=100, blank=True, null=True, help_text="Populate this field\
        with the username that should be used for authentication purposes")

    bluebin_sftp_password = models.CharField(
        max_length=100, blank=True, null=True, help_text="If applicable,\
        populate this field with the password that should be used for\
        authentication purposes")

    bluebin_sftp_port = models.IntegerField(
        blank=True, null=True, help_text="Enter the port that\
        should be used to make the connection (default=22 for SFTP)")

    bluebin_sftp_remote_file_path = models.CharField(
        max_length=100, blank=True, null=True, help_text="Enter the\
        file path of where the file should be written.  It's the path from the\
        default directory of the user we're signing in withself.\
        Don't forget the last slash.  Example: public_ftp/incoming/belmero/")

    bluebin_sftp_use_ssh_private_key = models.BooleanField(
        default=False, help_text="Check this checkbox to use the ssh\
        private key.  This must be added to the known_hosts file in the .ssh\
        folder")

    bluebin_sftp_private_key_password = models.CharField(
        max_length=100, blank=True, null=True, help_text="If applicable,\
        populate this field with the Private Key password that should be used\
        for authentication purposes.  Don't confuse this with the user\
        password")

    enable_email_integration = models.BooleanField(
        default=False, help_text="Check this checkbox to enable Email\
        file attachment integration.  Populate the corresponding email fields")

    email_integration_email_address = models.EmailField(
        blank=True, null=True, help_text="Enter the email address that the\
        file, as an attachment, should be emailed to")

    email_integration_cc_email_address = models.EmailField(
        blank=True, null=True, help_text="Enter the email address that the\
        file, as an attachment, should be CC'd to")

    email_integration_email_subject = models.CharField(
        max_length=100, blank=False,
        default="New Belmero-BlueBin Requisition File",
        help_text="This is the text that will show up in the subject of the\
            email.")

    email_integration_email_body_intro = models.TextField(
        blank=False, default="The following email contains\
            a new interface file of entered Requisitions.  They are\
            ready to be entered into your ERP",
        help_text="This is the text that will show up in the body of the\
            email.  INCLUDE a salutation like Hi or Hello")

    include_header_rows_in_file = models.BooleanField(
        default=False, help_text="Check this checkbox to create 2 header\
        rows; one for req header and one for req line.  First 2 rows in file")

    header_row_type_id_in_file = models.CharField(
        max_length=30, blank=False,
        default="H",
        help_text="Enter a value that will be the row type identifier on\
        every header row of data in the file.  This is the first field on\
        the header data row")

    line_row_type_id_in_file = models.CharField(
        max_length=30, blank=False,
        default="L",
        help_text="Enter a value that will be the row type identifier on\
        every Line row of data in the file.  This is the first field on\
        the line data row")

    file_name_prefix = models.CharField(
        max_length=90, blank=False,
        default="bluebin_reqs_",
        help_text="Enter a prefix for the beginning part of the file name.\
        The file name will be the prefix entered in this field plus\
        YYYYMMDD-HHMMSS (e.g. bluebin_reqs_20181122-170627.csv)")

    file_delimiter = models.CharField(
        max_length=1, blank=False,
        default=",",
        help_text="Enter a value to use to separate one field from another in\
        the file.")

    file_quote_character = models.CharField(
        max_length=1, blank=False,
        default='"',
        help_text="Enter a value to use to wrap around data fields that contain\
        the delimeter in their value.  This tells the program that the\
        delimiter in the data field should be ignored.")

    file_name_extension = models.CharField(
        max_length=10, blank=False,
        default='csv',
        help_text="Enter the extension (e.g. csv) that should be given to the\
        file.  No period is needed, enter just the file extension")

    entered_date = models.DateTimeField(default=timezone.now, blank=True,
                                        editable=False)

    updated_date = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return str("Requisition Integration Setup - Only ONE row")


class ReqIntegrationFileHdrFields(models.Model):
    sequence = models.IntegerField(primary_key=True, blank=False, null=False)

    field_name = models.CharField(max_length=90, blank=False, null=False)

    class Meta:
        ordering = ['sequence']

    def __str__(self):
        return str(self.sequence) + " - " + str(self.field_name)


class ReqIntegrationFileLineFields(models.Model):
    sequence = models.IntegerField(primary_key=True, blank=False, null=False)

    field_name = models.CharField(max_length=90, blank=False, null=False)

    class Meta:
        ordering = ['sequence']

    def __str__(self):
        return str(self.sequence) + " - " + str(self.field_name)


class ReqHeader(models.Model):
    # requisition header table
    requester_id = models.CharField(max_length=60, blank=True)  # passed in url

    business_unit = models.CharField(max_length=10)

    location = models.CharField(max_length=20)

    custom_field_c10 = models.CharField(max_length=10, blank=True)

    custom_field_c30 = models.CharField(max_length=30, blank=True)

    custom_field_c60 = models.CharField(max_length=60, blank=True)

    # automatically sets the date ONLY on the first save
    entered_date = models.DateTimeField(default=timezone.now, blank=True,
                                        editable=False)

    # automatically updates the date EVERY time it's saved
    updated_date = models.DateTimeField(auto_now=True, blank=True)

    # record status is either Add or future potential functionality of Change
    record_status = models.CharField(max_length=6, blank=True, default='Add')

    # the following fields along with record_status are used for the interfaced
    interfaced = models.BooleanField(default=False)

    last_interface_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return str(self.pk)


class ReqLine(models.Model):
    parent_id = models.ForeignKey(ReqHeader, related_name='req_line_set',
                                  on_delete=models.CASCADE)

    item_id = models.CharField(max_length=30, help_text="Enter the Item Id")

    bin = models.CharField(max_length=1, blank=True,
                           help_text="Enter the Bin Number")

    qty = models.IntegerField()

    custom_field_c10 = models.CharField(max_length=10, blank=True)

    custom_field_c30 = models.CharField(max_length=30, blank=True)

    custom_field_c60 = models.CharField(max_length=60, blank=True)

    # automatically sets the date ONLY on the first save
    entered_date = models.DateTimeField(default=timezone.now, blank=True,
                                        editable=False)

    # automatically updates the date EVERY time it's saved
    updated_date = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return 'Req ID: ' + str(self.parent_id) + '| Line ID: ' + str(self.pk)
