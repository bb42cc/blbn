from django import template
# from django.contrib.auth.models import Group

register = template.Library()

# add the following statement to the top of the Template to use these tags
# {% load returns_app_templatetags %}


# for Bootstrap purposes and other functionality
@register.filter(name='addattribute')
def addattribute(field, css):
    attrs = {}
    definition = css.split(',')

    for d in definition:
        if ':' not in d:
            attrs['class'] = d
        else:
            t, v = d.split(':')
            attrs[t] = v
    # print(dir(field))
    return field.as_widget(attrs=attrs)


# for conditional statement to determine if menu item should show based on the
# Group the User is assigned to
# use the following code in your template
# {% if request.user|has_group:"mygroup" %}
#    <p>User belongs to my group
# {% else %}
#    <p>User doesn't belong to mygroup</p>
# {% endif %}
@register.filter(name='has_group')
def has_group(user, group_name):
    return user.groups.filter(name=group_name).exists()
