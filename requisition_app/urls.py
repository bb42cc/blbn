from django.urls import path
from requisition_app import views
from django_filters.views import FilterView
from requisition_app.filters import RequisitionFilter
from django.contrib.auth.decorators import login_required

app_name = 'requisition_app'

urlpatterns = [
    path('requisition-request-create/<str:requester_id>/',
         views.requisition_request_create,
         name="requisition_request_create"),
    path('thank-you/',
         views.thank_you,
         name="thank_you"),
    path('requisition-search/',
         login_required(FilterView.as_view(
             filterset_class=RequisitionFilter,
             template_name='requisition_app/requisition_list.html')),
         name='requisition_search'),  # uses context filter.form and filter.qs
    path('requisition-request-inquiry/<int:pk>/',
         views.requisition_request_inquiry,
         name="requisition_request_inquiry"),
]
