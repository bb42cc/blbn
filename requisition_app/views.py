from django.shortcuts import render
from django.forms import inlineformset_factory
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required

from requisition_app import models
from requisition_app import forms


# Create your views here.
def requisition_request_create(request, requester_id):
    """
        Requisition Create
    """
    # retrieve installation values to identify hidden fields in template
    reqinstallation = models.ReqInstallation.objects.first()

    req_hdr = models.ReqHeader()  # empty parent model
    # create parent/hdr form
    req_hdr_form = forms.RequisitionHdrForm(
        instance=req_hdr, initial={'requester_id': requester_id})

    # instantiate inlineformset
    reqlineformset = inlineformset_factory(
        models.ReqHeader, models.ReqLine, form=forms.RequisitionLineForm,
        extra=reqinstallation.initial_number_of_lines)
    formset = reqlineformset(instance=req_hdr)

    if request.method == 'POST':
        # initial set
        req_hdr_form = forms.RequisitionHdrForm(request.POST, request.FILES)
        formset = reqlineformset(request.POST, request.FILES)

        if req_hdr_form.is_valid():
            parent = req_hdr_form.save(commit=False)  # save no commit
            parent.updated_date = timezone.now()  # was datetime.now()
            parent.requester_id = requester_id

            # set inlineformset using parent instance
            formset = reqlineformset(
                request.POST, request.FILES, instance=parent)

            if formset.is_valid():
                parent.save()
                formset.save()

                url = reverse('requisition_app:thank_you')
                return HttpResponseRedirect(url)

    return render(request, "requisition_app/requisition_request.html", {
        "form": req_hdr_form,
        "reqlineform": formset,
        "reqinstallation": reqinstallation,
        "requester_id": requester_id,
        "inquiry": False,
    })


def thank_you(request):
    """
        Thank you page
    """
    return render(request, 'requisition_app/thank_you.html')


@login_required
def requisition_request_inquiry(request, pk):
    """
        Requisition Inquiry
    """
    req_hdr = get_object_or_404(models.ReqHeader, pk=pk)

    # retrieve installation values to identify hidden fields in template
    reqinstallation = models.ReqInstallation.objects.first()

    req_hdr_form = forms.RequisitionHdrReadOnlyForm(
        instance=req_hdr)

    # instantiate inlineformset
    reqlineformset = inlineformset_factory(
        models.ReqHeader, models.ReqLine,
        form=forms.RequisitionLineReadOnlyForm)
    formset = reqlineformset(instance=req_hdr)

    # shares the same template as create but uses separate form to disable flds
    return render(request, "requisition_app/requisition_request.html", {
        "form": req_hdr_form,
        "reqlineform": formset,
        "reqinstallation": reqinstallation,
        "requester_id": req_hdr.requester_id,
        "inquiry": True,
    })


def get_req_hdr_columns():
    """
    Builds a tuple of all of the Req Hdr Fields
    Returns export_header
    """
    req_hdr_fields_qs = models.ReqIntegrationFileHdrFields.objects.all()
    req_hdr_fields_list = []
    for row in req_hdr_fields_qs:
        req_hdr_fields_list.insert(row.sequence, row.field_name)

    return req_hdr_fields_list


def get_req_line_columns():
    """
    Builds a tuple of all of the Req Line Fields except for id/pk
    Returns export_line
    """
    req_line_fields_qs = models.ReqIntegrationFileLineFields.objects.all()
    req_line_fields_list = []
    for row in req_line_fields_qs:
        req_line_fields_list.insert(row.sequence, row.field_name)

    return req_line_fields_list


def write_req_columns_to_file(writer, export_header, export_line):
    """
    Writes both the header and line columns to the file
    Returns writer
    """
    writer.writerow(export_header)
    writer.writerow(export_line)

    return writer


def get_and_write_req_data(export_data, req_line_fields_list, writer,
                           interfacetype, export_online_option, api_setup):
    """
    Using the header rows in export_data get the corresponding line rows
    (fields to export in req_line_fields_list) and then write both the header
    and line data to the file.  At the end update the record_hdr interface or
    export_online fields.
    interfacetype = "export" or "interface"
    export_online_option parm is only for "export" interfacetype (set to None)
    option values: = 1 (only new/changed rows) or 2 (all rows)
    Returns writer
    """
    for a in export_data:
        # header processing
        alist = list(a)

        # get hdr id from list for lookup and then remove it from list
        hdr_id = alist[0]
        alist.pop(0)

        # add header record identifier
        alist.insert(0, api_setup.header_row_type_id_in_file)
        a = tuple(alist)
        writer.writerow(a)

        # line processing
        lines = models.ReqLine.objects.filter(parent_id__pk=hdr_id). \
            values_list(*req_line_fields_list).order_by('id')
        for i in lines:
            ilist = list(i)
            # add line record identifier
            ilist.insert(0, api_setup.line_row_type_id_in_file)
            i = tuple(ilist)
            writer.writerow(i)
            # update record hdr that rows have been interfaced
            if interfacetype == 'interface':
                models.ReqHeader.objects.filter(pk=hdr_id).update(
                    interfaced=True,
                    last_interface_date=timezone.now()
                )
    return writer


def remove_leading_asterisk(field):
    '''
    removes leading * if it exists and returns value without *
    '''
    if str(field).find('*') != -1:
        field = str(str(field).split('*')[-1])
    return field
