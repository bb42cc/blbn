var default_options = {
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero:true
            }
        }]
    },
    tooltips: {
        displayColors: false
    }
};

var rbm_ctx = $("#returnsByMonthChart");
var rbm_labels = $(".returnsByMonth .month").toArray().map(function(e){
    return e.innerHTML;
}).reverse();
var rbm_data = $(".returnsByMonth .count").toArray().map(function(e){
    return e.innerHTML;
}).reverse();
new Chart(rbm_ctx, {
    type: 'bar',
    data: {
        labels: rbm_labels,
        datasets: [{
            label: '# of Returns',
            data: rbm_data,
            backgroundColor: [
                'rgba(92, 184, 92, 0.2)',
            ],
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1
        }]
    },
    options: default_options
});

var abm_ctx = $("#amountByMonthChart");
var abm_labels = $(".amountByMonth .month").toArray().map(function(e){
    return e.innerHTML;
}).reverse();
var abm_data = $(".amountByMonth .sum").toArray().map(function(e){
    return e.innerHTML.slice(1, e.innerHTML.length);
}).reverse();
new Chart(abm_ctx, {
    type: 'line',
    data: {
        labels: abm_labels,
        datasets: [{
            label: '$ Amount',
            data: abm_data,
            backgroundColor: [
                'rgba(92, 184, 92, 0.2)',
            ],
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1
        }]
    },
    options: default_options
});

var abr_ctx = $("#amountByReasonChart");
var abr_labels = $(".amountByReason .reasonCode").toArray().map(function(e){
    return e.innerHTML.slice(0, e.innerHTML.length-1);
}).reverse();
var abr_data = $(".amountByReason .sum").toArray().map(function(e){
    return e.innerHTML.slice(1, e.innerHTML.length);
}).reverse();
new Chart(abr_ctx, {
    type: 'pie',
    data: {
        labels: abr_labels,
        datasets: [{
            label: '$ Amount',
            data: abr_data,
            backgroundColor: abr_labels.map(function(e, i) {
                return ['#e74c3c', '#27ae60', '#3498db', '#f1c40f', '#8e44ad'][i % 5];
            }),
        }]
    }
});

var abs_ctx = $("#amountBySupplierChart");
var abs_labels = $(".amountBySupplier .supplierName").toArray().map(function(e){
    return e.innerHTML;
}).reverse();
var abs_data = $(".amountBySupplier .sum").toArray().map(function(e){
    return e.innerHTML.slice(1, e.innerHTML.length);
}).reverse();
new Chart(abs_ctx, {
    type: 'line',
    data: {
        labels: abs_labels,
        datasets: [{
            label: '$ Amount',
            data: abs_data,
            backgroundColor: [
                'rgba(92, 184, 92, 0.2)',
            ],
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1
        }]
    },
    options: default_options
});

var abs_ctx = $("#amountBySupplierChart");
var abs_labels = $(".amountBySupplier .supplierName").toArray().map(function(e){
    return e.innerHTML;
}).reverse();
var abs_data = $(".amountBySupplier .sum").toArray().map(function(e){
    return e.innerText.slice(1, e.innerText.length);
}).reverse();
var myChart = new Chart(abs_ctx, {
    type: 'line',
    data: {
        labels: abs_labels,
        datasets: [{
            label: '$ Amount',
            data: abs_data,
            backgroundColor: [
                'rgba(92, 184, 92, 0.2)',
            ],
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1
        }]
    },
    options: default_options
});

var abu_ctx = $("#amountByUserChart");
var abu_labels = $(".amountByUser .requesterName").toArray().map(function(e){
    return e.innerHTML;
}).reverse();
var abu_data = $(".amountByUser .sum").toArray().map(function(e){
    return e.innerText.slice(1, e.innerText.length);
}).reverse();
var myChart = new Chart(abu_ctx, {
    type: 'bar',
    data: {
        labels: abu_labels,
        datasets: [{
            label: '$ Amount',
            data: abu_data,
            backgroundColor: [
                'rgba(92, 184, 92, 0.2)',
            ],
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1
        }]
    },
    options: default_options
}); 